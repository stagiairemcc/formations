import React from 'react';
import { Link } from 'react-router-dom';

const logoUrl = require('../icons/logo.png');

const Logo = ({ changeMenuMarker }) => (
  <div className="navbar-logo-container">
    <a style= {{width :"380px",height:"380px",transform: "scale(0.36)",}} href='https://dash-14a2d.firebaseapp.com/' onClick={() => changeMenuMarker('Formations')}>
      <img style= {{width :"100%",height:"100%",}}  src={logoUrl} alt='navbar-logo' />
    </a>
  </div>
);

export { Logo };

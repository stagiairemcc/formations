import React from 'react';
import "../../node_modules/video-react/dist/video-react.css";
//import videos from '../video/appel-externe.mp4'
import video2 from '../video/appel-interne.mp4'
import video3 from '../video/appel-attente.mp4'
//import video4 from '../video/connexion.mp4'
import video5 from '../video/Repond-raccrocher.mp4'
import { Fade } from 'react-slideshow-image';
import ReactPlayer from 'react-player';
//import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
<link rel="stylesheet" href="/css/video-react.css" />
const imageUrl = 'https://image.tmdb.org/t/p/';
const size = 'original';
   
  const fadeProperties = {
    duration: 8000,
    transitionDuration: 500,
    infinite: false,
    indicators: true
  }
  const VideoJumbotron = ({movies}) => {
    return (
        <Fade {...fadeProperties}>
          <div className="each-fade">
            <div className="image-container">
            {/* <Player  
                fluid={false}
                 width={1400}
                 height={500} >
                <source src= {video2} />
             </Player> */}
               <ReactPlayer
                url={video2}
                  className='react-player'
                
                   controls={true}
                   width='75%'
                   height='100%'
                />
            </div>
            <h2>Effectuer un appel interne</h2>
          </div>
          <div className="each-fade">
            <div className="image-container">
            {/* <Player  
                fluid={false}
                 width={1400}
                 height={500} >
                <source src= {video3} />
             </Player> */}
               <ReactPlayer
                url={video3}
                  className='react-player'
                
                   controls={true}
                   width='75%'
                   height='100%'
                />
            </div>
            <h2>Mettre un appel en attente</h2>
          </div>
         
          
          <div className="each-fade">
            <div className="image-container">
            {/* <Player  
                fluid={false}
                 width={1400}
                 height={500} >
                <source src= {video5} />
             </Player> */}
               <ReactPlayer
               className='react-player'
                url={video5}
                   controls={true}
                   width='75%'
                   height='100%'
                />
            </div>
            <h2>Répondre à un appel et raccrocher</h2>
          </div>
        </Fade>
      )
    }

export { VideoJumbotron };

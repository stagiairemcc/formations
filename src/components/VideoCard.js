import React from 'react';
import { Button } from './index';
import ModalVideo from 'react-modal-video'
import { Player } from 'video-react';
import ReactPlayer from 'react-player';
<link
  rel="stylesheet"
  href="https://video-react.github.io/assets/video-react.css"
/>
const TRUNCATE_LENGTH = 100;


const VideoCard = ({ movie, movies,favoriteList,onAddListPressed
 }) => (
  
<div
  className="movie-card "
  //data-path={true}
  style={{
    backgroundColor: '#202020',
  }}
  data-path={true}
  onClick={e=>{movies.condition= e.currentTarget.dataset.path}}
  //onButtonPressed={()=>movies.condition =true}
    //onClick={e=>{movies.VideoJumbotron.backdrop_path= e.currentTarget.dataset.path

    // }}
      >
 <Player  fluid={false}
        width={500}
        height={250} >
      <source src= {movie.src} 
      />
    </Player>
  </div>
);

export { VideoCard };

import React from 'react';
import { Link } from 'react-router-dom';

const Menu = ({ changeMenuMarker, page }) => (
  <div className="navbar-menu-option-container">
    <Link to='/' onClick={() => changeMenuMarker('Formations')}>
      <span
        className="navbar-menu-option"
        style={{ borderColor: page === 'Formations' ? '#B8130D' : null }}
      >
       
      </span>
    </Link>
   
  </div>
);

export { Menu };

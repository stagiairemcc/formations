import React from 'react';
import { Button } from './index';

import { Player } from 'video-react';
const imageUrl = 'https://image.tmdb.org/t/p/';
const size = 'w500';
const TRUNCATE_LENGTH = 100;


const MovieCard = ({ movie, movies,favoriteList,onAddListPressed
 }) => (
  
  <div
  className="movie-card "
  data-path={movie.backdrop_path}
  data-title={movie.original_title}
  data-desc={movie.overview}
  data-url={movie.url}
  data-condition={movies.condition}
  style={{
    backgroundColor: '#202020',
    backgroundImage: `url(${movie.backdrop_path})`,
    
  }}
 
    onClick={e=>{movies.movieJumbotron.backdrop_path= e.currentTarget.dataset.path,
    movies.movieJumbotron.original_title= e.currentTarget.dataset.title,
    movies.movieJumbotron.overview= e.currentTarget.dataset.desc,
    movies.movieJumbotron.url= e.currentTarget.dataset.url,
    movies.condition = false
    //le onClick nous permet de savoir exactement là où exactement on a cliqué et récupéré l'objet
     }}
  
   
    
     >
    <div className="movie-card-container">
      <div className="movie-card-text">
       
        <div className="movie-card-info">
          
          
        </div>
        <div className="movie-card-description">{movie.overview ? movie.overview.substring(0, TRUNCATE_LENGTH) + '...' : 'Esse filme ainda não tem uma descrição.'}</div>
      </div>
      <div className="movie-card-button-container">
        <Button
          buttonStyleOptions="round-button"
          iconStyleOptions="fa-fw"
          icon={favoriteList.filter(l => l.id === movie.id).length ? 'check' : 'plus'}
          onButtonPressed={() => onAddListPressed(movie)}
        />
      </div>
    </div>
  </div>
);

export { MovieCard };

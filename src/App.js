import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { NavBar } from './components';
import Home from './view/Home';
import site1 from './img/udemy.png'
import site2 from './img/saylor.png'
import site3 from './img/coursera.png'
import site4 from './img/udacity.png'
import site5 from './img/google.jpg'
import site6 from './img/GSuiteTraining.png'
import site7 from './img/LearningLab.png'
//import videos from './video/appel-externe.mp4'
import video2 from './video/appel-interne.mp4'
import video3 from './video/appel-attente.mp4'
//import video4 from './video/connexion.mp4'
import video5 from './video/Repond-raccrocher.mp4'


import './App.css'

class NetflixCloneApp extends React.Component {
  state = {
    
    movies: {
      condition: true,
      open:true,
      movieJumbotron: {
        "backdrop_path": site4,
        "original_title": "Udacity",
        "overview": "Master in-demand skills with our online learning programs available anytime, from any device. Build and design amazing projects. Earn a valued credential. Launch your career in Data Science, Machine Learning, AI, Android, iOS, Marketing and more. Be in demand.",
       "url":"https://eu.udacity.com/"
      },
      VideoJumbotron: {
        "src":""
      },
      learninglab: [{
        "backdrop_path": site7,
        "original_title": "Learning Lab",
        "overview": "Learning Lab est un centre d'alphabétisation pour adultes et familles avec jeunes enfants. Les cours sont basés sur les besoins individuels des étudiants et proposent des programmes d'apprentissage en lecture, écriture, mathématiques et anglais. Learning Lab propose également une préparation aux examens de citoyenneté et GED.",
       "url":"http://learninglabinc.org/"
      },
      {
        "backdrop_path": site6,
        "original_title": "Suite Training",
        "overview": "",
       "url":"https://portal.synergyse.com/a/#videos/en/drive"
      },],
      MesFormation: [
      {
        "src":video2
      },
      {
        "src":video3
      },
      {
        "src":video5
      },
    ],
      lastestMovies: [
        {
          "backdrop_path": site5,
          "original_title": "Google Formation",
          "overview": "Faites évoluer votre carrière ou votre activité à votre rythme grâce à des formations flexibles et personnalisées, conçues pour renforcer votre confiance et vous aider à réussir.",
         
          "url":"https://learndigital.withgoogle.com/ateliersnumeriques"},
         {
          "backdrop_path": site1,
          "original_title": "Udemy",
          "overview": "Étudiez à tout moment le sujet que vous souhaitez. Découvrez des milliers de cours dès aujourd'hui.",
         
          "url":"https://www.udemy.com/"
        }, {
          "backdrop_path": site2,
          "original_title": "Saylor",
          "overview": "Build new skills or work toward a degree at your own pace with free Saylor Academy courses.",
         
          "url":"https://www.saylor.org/"
        },{
          "backdrop_path": site3,
          "original_title": "Coursera",
          "overview": "Formation 100 % en ligne auprès des meilleures universités et entreprises du monde",
         
          "url":"https://www.coursera.org/"},
          ,{
            "backdrop_path": site4,
            "original_title": "Udacity",
            "overview": "Maîtrisez les compétences recherchées grâce à nos programmes d'apprentissage en ligne disponibles à tout moment, depuis n'importe quel appareil. Construire et concevoir des projets étonnants. Gagnez un titre de valeur. Lancez votre carrière dans les domaines suivants: science des données, apprentissage automatique, intelligence artificielle, Android, iOS, marketing, etc. Être en demande.",
           
            "url":"https://eu.udacity.com/"},
            
    ]
    },
    favoriteList: [],
    fetchedMovies: [],
    isInputClosed: true,
    avatarPhoto: '',
   
  }

  componentDidMount() {
   // Movies.getMostPopular().then(res => this.setState({ movieJumbotron: res }));
   //Movies.getInTheater().then(res => this.setState({ movies: { ...this.state.movies, lastestMovies: res } })); 
  //  Movies.getInTheater().then(res => console.log(res) );
  //   Movies.getMostPopular().then(res => console.log(res));
  }

  toggleMovieInFavoriteList = movie => {
    const { favoriteList } = this.state;
    let index = favoriteList.map(l => l.id).indexOf(movie.id);

    if (index === -1) {
      this.setState({ favoriteList: [...favoriteList, movie] });
    } else {
      this.setState({ favoriteList: [
        ...favoriteList.slice(0, index),
        ...favoriteList.slice(index + 1)
      ]});
    }
  }

  doSearch = query => {
   
  }
  

  render() {
    return (
      <div className="app">
        <NavBar
          onSearchMovies={query => this.doSearch(query)}
          onCollapseInputHandler={() => this.setState({ isInputClosed: true })}
          onExpandInputHandler={() => this.setState({ isInputClosed: false })}
        />
        <Route exact path='/' render={() => (
          !this.state.isInputClosed && this.state.fetchedMovies.length
          ? <Redirect to="/search" />
          : <Home
              movies={this.state.movies}
              movieJumbotron={this.state.movieJumbotron}
              favoriteList={this.state.favoriteList}
              //condition = {this.state.condition}
              onAddListPressed={movie => this.toggleMovieInFavoriteList(movie)}
            />
        )} />
        )} />
      </div>
    )
  }
}
export default NetflixCloneApp;

import React from 'react';
import { MovieCard, Jumbotron, Carousel ,VideoCard, VideoJumbotron} from '../components';
//La en utilise (boolean ?(si c'est vrai) action : ( sinon) action); 
const Home = ({ movieJumbotron, favoriteList, onAddListPressed, movies }) => movies.condition ?(
  
  <div className="home-container" >
 {/* <Jumbotron
     movie={movies.movieJumbotron}
     favoriteList={favoriteList}
     onAddListPressed={movie => onAddListPressed(movie)}
    />  */}
    <VideoJumbotron
      movie={movies.movieJumbotron}
      movies = {movies}
      favoriteList={favoriteList}
      onAddListPressed={movie => onAddListPressed(movie)}
    />
    {/* <div 
  className="movie-card-MesFormation">
  <h2> Mes Formations</h2>
  <img width ="470" src={MesFormation} onClick={movies.condition=true}></img></div> */}
      
        
        
    
   
    <Carousel title="Mes formations" >
    {movies.MesFormation.map(movie => (
        <VideoCard
          movie={movie}
           movies = {movies}
          favoriteList={favoriteList}
         onAddListPressed={movie => onAddListPressed(movie)}
        />
        
      ))}

    </Carousel>
    
    <Carousel title="Formation proposée par l'entreprise" >
      {movies.learninglab.map(movie => (
        <MovieCard
          movie={movie}
           movies = {movies}
          favoriteList={favoriteList}
         onAddListPressed={movie => onAddListPressed(movie)}
        />
        
      ))}
    </Carousel>
    <Carousel title="Autre formations" >
      {movies.lastestMovies.map(movie => (
        <MovieCard
          movie={movie}
          movies = {movies}
          favoriteList={favoriteList}
         onAddListPressed={movie => onAddListPressed(movie)}
        />
        
      ))}
    </Carousel>
    

  </div>
): (
  
  <div className="home-container">
 <Jumbotron

     movie={movies.movieJumbotron}
     movies = {movies}
     favoriteList={favoriteList}
     onAddListPressed={movie => onAddListPressed(movie)}
    /> 
    {/* <VideoJumbotron
      movie={movies.movieJumbotron}
      favoriteList={favoriteList}
      onAddListPressed={movie => onAddListPressed(movie)}
    /> */}
    <Carousel title="Mes formation" >
    {movies.MesFormation.map(movie => (
        <VideoCard
          movie={movie}
           movies = {movies}
          favoriteList={favoriteList}
          onAddListPressed={movie => onAddListPressed(movie)}
        />
        
      ))}

    </Carousel>
    {/* <div 
  className="movie-card-MesFormation">
  <h2> Mes Formations</h2>
  <img width ="470" src={MesFormation} onClick={movies.condition=true}></img></div> */}

    <Carousel title="Formation proposée par l'entreprise" >
      {movies.learninglab.map(movie => (
        <MovieCard
          movie={movie}
           movies = {movies}
          favoriteList={favoriteList}
          onAddListPressed={movie => onAddListPressed(movie)}
        />
        
      ))}
    </Carousel>
    <Carousel title="Autre formations" >
      {movies.lastestMovies.map(movie => (
        <MovieCard
          movie={movie}
          movies = {movies}
          favoriteList={favoriteList}
          onAddListPressed={movie => onAddListPressed(movie)}
        />
        
      ))}
    </Carousel>
    

  </div>
);

export default Home;

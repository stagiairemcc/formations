# Installation

Follow these steps

* `git clone https://gitlab.com/stagiairemcc/formations.git`
* `npm install` or `yarn`
* `npm run start` or `yarn start`

# About the application

* `react-router-dom` found [here](https://github.com/ReactTraining/react-router).
* `react-items-carousel` found [here](https://github.com/bitriddler/react-items-carousel).
* `video-react` found [here](https://www.npmjs.com/package/video-react).
* `react-slideshow-image` found [here](https://www.npmjs.com/package/react-slideshow-image).
* `react-player` found [here](https://www.npmjs.com/package/react-player).


# deploy

* the deployment is automatic, for more information found [here](https://firebase.google.com/docs/cli)
